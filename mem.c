//
// Created by jakev on 17.12.2020.
//

#include "mem.h"

static struct mem *heap = 0;

static void *mem_init(void *address, size_t size, bool fixed) {
    if (size < BLOCK_MIN_SIZE) size = BLOCK_MIN_SIZE;
    return mmap(address, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | (fixed ? MAP_FIXED : 0), -1, 0);
}

static void blocks_merge(struct mem *const b1, struct mem *b2) {
    b1->next = b2->next;
    b1->capacity += sizeof(struct mem) + b2->capacity;
    b2->capacity = 0;
    b2->next = NULL;
    b2->is_free = 0;
    b2 = NULL;
}

static void block_split(struct mem *const block, size_t query) {
    struct mem *new_block = (struct mem *) ((char *) block + sizeof(struct mem) + query);
    new_block->is_free = true;
    new_block->next = block->next;
    new_block->capacity = block->capacity - sizeof(struct mem) - query;

    block->capacity = query;
    block->next = new_block;
    block->is_free = false;
}

void *heap_init(size_t initial_size) {
    if (initial_size < HEAP_PAGE_SIZE) initial_size = HEAP_PAGE_SIZE;
    heap = mem_init(HEAP_START, initial_size, false);
    heap->next = NULL;
    heap->capacity = initial_size - sizeof(struct mem);
    heap->is_free = true;

    return heap;
}


void *_malloc(size_t query) {
    struct mem *block, *new_page, *prev;
    size_t page_size;
    void *page_addr;

    if (query == 0) return NULL;
    if (query < BLOCK_MIN_SIZE) query = BLOCK_MIN_SIZE;
    if (heap == NULL) heap = heap_init(HEAP_PAGE_SIZE);

    for (block = heap; block != NULL; block = block->next) {
        prev = block;
        if (block->is_free && query + sizeof(struct mem) + BLOCK_MIN_SIZE <= block->capacity ) {
            block_split(block, query);
            return (char *) block + sizeof(struct mem);
        };
    }

    page_size = query - prev->capacity + sizeof(struct mem) + BLOCK_MIN_SIZE;
    page_addr = (char *) prev + prev->capacity + sizeof(struct mem);
    if (mem_init(page_addr, page_size, true) != MAP_FAILED) {
        prev->capacity += page_size;
        block_split(prev, query);
        return (char *) prev + sizeof(struct mem);
    } else {
        page_size = query + 2 * sizeof(struct mem) + BLOCK_MIN_SIZE;
        new_page = mem_init(0, page_size, false);
        if (new_page == NULL) return NULL;
        prev->next = new_page;
        new_page->next = NULL;
        new_page->capacity = page_size - sizeof(struct mem);
        new_page->is_free = true;
        block_split(new_page, query);
        return (char *) new_page + sizeof(struct mem);

    }
}

void *_realloc(void *ptr, size_t new_size) {
    void *new_ptr;
    struct mem *current = ptr - sizeof(struct mem);
    if (new_size == 0 | ptr == 0) return NULL;
    if (new_size < BLOCK_MIN_SIZE) new_size = BLOCK_MIN_SIZE;
    if (current->capacity >= new_size + sizeof(struct mem) + BLOCK_MIN_SIZE) {
        block_split(current, new_size);
        return ptr;
    } else{
        new_ptr = _malloc(new_size);
        if(new_ptr ==NULL) return NULL;
        memcpy(new_ptr, ptr, current->capacity);
        _free(ptr);
        return new_ptr;
    }
}

void _free(void *mem) {
    struct mem *current = (struct mem *) ((char *) mem - sizeof(struct mem));
    current->is_free = true;
    while (current->next != NULL) {
        if (current->is_free == true && current->next->is_free == true &&
            (char *) current->next == (char *) current + sizeof(struct mem) + current->capacity)
            blocks_merge(current, current->next);
        else current = current->next;
    }

}

