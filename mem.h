//
// Created by jakev on 17.12.2020.
//

#ifndef L7_MEM_H
#define L7_MEM_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <string.h>

#define HEAP_START ((void*)0x04040000)
#define BLOCK_MIN_SIZE 32
#define HEAP_PAGE_SIZE (4*1024)


struct mem;

#pragma pack(push, 1)
struct mem {
    struct mem *next;
    size_t capacity;
    bool is_free;
};
#pragma pack(pop)

void *_malloc(size_t query);

void *_realloc(void *ptr, size_t new_size);

void _free(void *mem);

void *heap_init(size_t initial_size);

#define DEBUG_FIRST_BYTES 4

void memalloc_debug_struct_info(FILE *f,
                                struct mem const *const address);

void memalloc_debug_heap(FILE *f, struct mem const *ptr);

#endif //L7_MEM_H
