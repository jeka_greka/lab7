all: main

main: main.c mem.c mem_debug.c
	gcc main.c mem.c mem_debug.c -o main

clean:
	rm -f main
