#include "mem.h"

int main() {
    char *new = _malloc(3);
    memalloc_debug_struct_info(stdout, (struct mem *) (new - sizeof(struct mem)));
    char *new1 = _malloc(500);
    memalloc_debug_struct_info(stdout, (struct mem *) (new1 - sizeof(struct mem)));
    char *new2 = _malloc(400);
    memalloc_debug_struct_info(stdout, (struct mem *) (new2 - sizeof(struct mem)));
    _free(new2);
    memalloc_debug_struct_info(stdout, (struct mem *) (new2 - sizeof(struct mem)));

    int *new3 = _malloc(3514);
    new3[0] = 123;
    memalloc_debug_struct_info(stdout, (struct mem *) ((char *) new3 - sizeof(struct mem)));
    char *new4 = _realloc(new3, 4000);
    memalloc_debug_struct_info(stdout, (struct mem *) (new4 - sizeof(struct mem)));
    memalloc_debug_struct_info(stdout, (struct mem *) ((char *) new3 - sizeof(struct mem)));
}
